all: build docker

all-dev: build-dev docker

build:
	go build -ldflags="-s -w"
	upx --brute weekday-api

build-dev:
	go build

docker:
	docker build -t weekday-api .

clean:
	go clean
	rm -f coverage.out

test:
	go test -v "./..." -coverprofile="coverage.out"
	go tool cover -func="coverage.out"
