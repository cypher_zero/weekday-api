package main

import (
	"encoding/json"
	"testing"
)

func CheckDays(msg string) bool {
	// Slice of valid days of the week
	var validDays []string = []string{"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"}
	var match bool = false
	for i := range validDays {
		if msg == validDays[i] {
			match = true
			break
		}
	}

	return match
}

func TestGetDay(t *testing.T) {
	msg := GetDay()

	if !CheckDays(msg) {
		t.Fatalf(`GetDay() does not return a valid day of the week. Returned: %v`, msg)
	}
}

func TestGetDayJSON(t *testing.T) {
	msg := GetDayJSON()
	msgJSONmap := map[string]string{}
	err := json.Unmarshal(msg, &msgJSONmap)

	if err != nil {
		t.Fatalf(`GetDayJSON() - unable to unmarshal JSON. Err: %v`, err)
	}

	if _, val := msgJSONmap["today"]; !val {
		t.Fatalf(`GetDayJSON() has no value for 'today'.`)
	}

	if !CheckDays(msgJSONmap["today"]) {
		t.Fatalf(`GetDayJSON() does not return a valid day of the week. Returned: %v`, msgJSONmap["today"])
	}
}
