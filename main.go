package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"sync/atomic"
	"syscall"
	"time"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

// apiPort is the port the API server will present on. 10000 by default
var apiPort int = 10000

func GetDay() string {
	return time.Now().Weekday().String()
}

func GetDayJSON() []byte {
	var todayMap map[string]string = make(map[string]string)
	todayMap["today"] = GetDay()
	todayJSON, err := json.MarshalIndent(todayMap, "", "  ")
	if err != nil {
		log.Println("[GetDayJSON] Error marshalling JSON:", err)
	}

	return todayJSON
}

func TodayJSON(w http.ResponseWriter, r *http.Request) {
	log.Println("Endpoint hit: TodayJSON")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	_, err := w.Write(GetDayJSON())
	if err != nil {
		log.Println("[TodayJSON] Failed to return GetDayJSON")
	}
}

// HealthZ is a liveness probe.
func HealthZ(w http.ResponseWriter, _ *http.Request) {
	log.Println("Endpoint hit: HealthZ")
	w.WriteHeader(http.StatusOK)
}

// ReadyZ is a readiness probe.
func ReadyZ(isReady *atomic.Value) http.HandlerFunc {
	return func(w http.ResponseWriter, _ *http.Request) {
		if isReady == nil || !isReady.Load().(bool) {
			http.Error(w, http.StatusText(http.StatusServiceUnavailable), http.StatusServiceUnavailable)
			return
		}
		log.Println("Endpoint hit: ReadyZ")
		w.WriteHeader(http.StatusOK)
	}
}

func NewREST() *mux.Router {
	// Test for readiness
	isReady := &atomic.Value{}
	isReady.Store(false)
	go func() {
		time.Sleep(10 * time.Second)
		msg := GetDayJSON()
		msgJSONmap := map[string]string{}
		json.Unmarshal(msg, &msgJSONmap)
		_, val := msgJSONmap["today"]
		log.Println("ReadyZ probe is set.")
		isReady.Store(val)
	}()

	// Start handlers
	r := mux.NewRouter()
	r.HandleFunc("/", TodayJSON)
	r.HandleFunc("/healthz", HealthZ)
	r.HandleFunc("/readyz", ReadyZ(isReady))

	return r
}

func HandleRequests(port int) {
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt, syscall.SIGTERM)

	router := NewREST()
	credentials := handlers.AllowCredentials()
	methods := handlers.AllowedMethods([]string{"GET"})
	origins := handlers.AllowedOrigins([]string{"*"})
	srv := &http.Server{
		Addr:    ":" + fmt.Sprint(port),
		Handler: handlers.CORS(credentials, methods, origins)(router),
	}
	go func() {
		log.Fatal(srv.ListenAndServe())
	}()
	log.Println("The service is ready to listen and serve")

	killSignal := <-interrupt
	switch killSignal {
	case os.Interrupt:
		log.Print("Got SIGINT...")
	case syscall.SIGTERM:
		log.Print("Got SIGTERM...")
	}

	log.Print("The service is shutting down...")
	srv.Shutdown(context.Background())
	log.Print("Done")
}

func main() {
	log.Println("Starting api handler")
	HandleRequests(apiPort)
}
