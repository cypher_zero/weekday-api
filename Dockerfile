FROM alpine

RUN apk update --no-cache && \
    apk upgrade --no-cache

RUN apk update --no-cache && \
    apk upgrade --no-cache && \
    apk add --no-cache libc6-compat

RUN addgroup -S weekday-api && \
    adduser  -S -g "weekday-api" weekday-api weekday-api

RUN mkdir -p /etc/weekday-api && \
    mkdir -p /var/lib/weekday-api && \
    chown -R weekday-api:weekday-api /etc/weekday-api && \
    chown -R weekday-api:weekday-api /var/lib/weekday-api

ADD ./weekday-api /bin/weekday-api

RUN chown root:root /bin/weekday-api && \
    chmod 755 /bin/weekday-api

USER weekday-api

ENTRYPOINT [ "/bin/sh", "-c" ]
CMD [ "/bin/weekday-api" ]
